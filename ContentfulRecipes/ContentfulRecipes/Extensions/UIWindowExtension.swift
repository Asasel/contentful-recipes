//
//  UIWindowExtension.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {

    var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(viewController: rootViewController)
    }

    static func getVisibleViewControllerFrom(viewController: UIViewController?) -> UIViewController? {
        if let navigationController = viewController as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(viewController: navigationController.visibleViewController)
        }
        if let tabBarController = viewController as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(viewController: tabBarController.selectedViewController)
        }
        if let pvc = viewController?.presentedViewController {
            return UIWindow.getVisibleViewControllerFrom(viewController: pvc)
        }
        return viewController
    }
}
