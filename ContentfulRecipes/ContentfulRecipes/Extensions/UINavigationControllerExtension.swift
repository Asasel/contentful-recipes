//
//  UINavigationControllerExtension.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    static func instantiateNavigationController(rootViewController: UIViewController, title: String = "") -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.viewControllers.first?.navigationItem.title = title
        navigationController.navigationBar.tintColor = .black
        navigationController.restorationIdentifier = typeName(type: rootViewController.self)
        return navigationController
    }
}
