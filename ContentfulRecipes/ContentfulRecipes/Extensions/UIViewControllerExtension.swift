//
//  UIViewControllerExtension.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    class var identifier: String {
        return String(describing: self)
    }

    static func instantiateFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>(_ viewType: T.Type) -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib(self)
    }
}
