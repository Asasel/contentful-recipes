//
//  DetailPageAssembly.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class DetailPageAssembly {
    
    // MARK: - Properties
    
    private var presenter: DetailPagePresenter

    private(set) var viewController: DetailPageViewController

    // MARK: - Constructor
    
    init(with viewModel: DetailPageViewModel) {
        self.viewController = DetailPageViewController.instantiateFromNib()
        self.presenter = DetailPagePresenter(with: self.viewController, viewModel: viewModel)
        self.viewController.presenter = self.presenter
    }
    
    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: DetailPageAssembly")
    }
}
