//
//  DetailPageViewModel.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct DetailPageViewModel {
    
    let title: String
    
    let imageURL: URL?
    
    let description: String
    
    let chefName: String
    
    var chefDescription: String {
        return chefName.isEmpty ? "" : "Chef name: \(chefName)"
    }
    
    let tags: [String]    
}
