//
//  DetailPageProtocols.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

protocol DetailPagePresenterToView: class {   // Presenter calls, View listens.
        
    func updateContenView(with viewModel: DetailPageViewModel)
}

protocol DetailPageViewToPresenter: class {   // View calls, Presenter listens.
    
    func configureDetailPageViewContent()
}
