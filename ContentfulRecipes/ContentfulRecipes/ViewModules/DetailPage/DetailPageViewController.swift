//
//  DetailPageViewController.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit
import Nuke

class DetailPageViewController: UIViewController {
    
    // MARK: - Properties

    @IBOutlet private weak var imageView: UIImageView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    @IBOutlet private weak var chefNameLabel: UILabel!
    
    @IBOutlet private weak var tagsStackView: UIStackView!
    
    private var tagButtons = [UIButton]()
        
    var presenter: DetailPageViewToPresenter?

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.configureDetailPageViewContent()
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: DetailPageViewController")
    }

    // MARK: - Private functions
    
    private func configureTagsView(tags: [String]) {
        if tags.isEmpty {
            return
        }
        tags.forEach { tag in
            let tagButton = UIButton()
            tagButton.setTitle(tag, for: .normal)
            tagButton.sizeToFit()
            tagButton.backgroundColor = .systemGreen
            tagButton.layer.cornerRadius = 4
            tagsStackView.addArrangedSubview(tagButton)
        }
    }
    
    private func configureImageView(with url: URL?) {
        imageView.layer.cornerRadius = 8
        guard let url = url else {
            return
        }
        Nuke.loadImage(with: url, into: imageView)
    }
}

extension DetailPageViewController: DetailPagePresenterToView {
    
    func updateContenView(with viewModel: DetailPageViewModel) {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        chefNameLabel.text = viewModel.chefDescription
        configureTagsView(tags: viewModel.tags)
        configureImageView(with: viewModel.imageURL)
    }
}
