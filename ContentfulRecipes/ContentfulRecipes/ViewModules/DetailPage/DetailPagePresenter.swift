//
//  DetailPagePresenter.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class DetailPagePresenter {
    
    // MARK: - Properties
    
    private weak var contentViewController: DetailPagePresenterToView?
        
    private var viewModel: DetailPageViewModel

    // MARK: - Constructor
    
    init(with contentViewController: DetailPagePresenterToView, viewModel: DetailPageViewModel) {
        self.contentViewController = contentViewController
        self.viewModel = viewModel
    }
    
    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: DetailPagePresenter")
    }
}

extension DetailPagePresenter: DetailPageViewToPresenter {
    
    func configureDetailPageViewContent() {
        contentViewController?.updateContenView(with: viewModel)
    }
}
