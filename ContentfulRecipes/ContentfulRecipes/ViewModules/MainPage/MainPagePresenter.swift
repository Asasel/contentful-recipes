//
//  MainPagePresenter.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class MainPagePresenter {
    
    // MARK: - Properties
        
    private weak var router: MainPagePresenterToRouter?

    private weak var interactor: MainPagePresenterToInteractor?
    
    private var recipeInfoList = [RecipeInfo]()
    
    private var viewModels: [MainPageViewModel] {
        return recipeInfoList.compactMap {
            MainPageViewModel(id: $0.id, recipeTitle: $0.recipeTitle, imageURL: $0.imageURL)
        }
    }

    // MARK: - Constructor
    
    init(router: MainPagePresenterToRouter, interactor: MainPagePresenterToInteractor) {
        self.router = router
        self.interactor = interactor
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: MainPagePresenter")
    }
}

extension MainPagePresenter: MainPageViewToPresenter {
    
    func updateRecipeListView(completion: @escaping MainPageUpdateViewClosure) {
        interactor?.updateRecipeList(completion: { [weak self] recipesRaw, chefsRaw, tagsRaw in
            let adaptor = RecipeInfoAdaptor(recipesRaw: recipesRaw, chefsRaw: chefsRaw, tagsRaw: tagsRaw)
            guard let self = self else {
                return
            }
            self.recipeInfoList = adaptor.recipeInfoList
            completion(self.viewModels)
        })
    }
    
    func selectRecipe(with id: String) {
        guard let recipeInfo = recipeInfoList.first(where: { $0.id == id }) else {
            return
        }
        let viewModel = DetailPageViewModel(title: recipeInfo.recipeTitle,
                                            imageURL: recipeInfo.imageURL,
                                            description: recipeInfo.description, chefName: recipeInfo.chefName,
                                            tags: recipeInfo.tags)
        router?.navigate(to: .detailPage, with: viewModel)
    }
}
