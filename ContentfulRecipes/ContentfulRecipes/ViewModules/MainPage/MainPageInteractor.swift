//
//  MainPageInteractor.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class MainPageInteractor {
    
    // MARK: - Properties
    
    private var recipeService: RecipeService

    // MARK: - Constructor
    
    init() {
        self.recipeService = RecipeService()
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: MainPageInteractor")
    }

    // MARK: - Private functions
}

extension MainPageInteractor: MainPagePresenterToInteractor {
    
    func updateRecipeList(completion: @escaping RecipeInfoListInfoClosure) {
        recipeService.fetchRecipes(completion: completion)
    }
}
