//
//  Recipe.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Contentful

final class Recipe: EntryDecodable, FieldKeysQueryable {
    
    static let contentTypeId: String = "recipe"
    
    let id: String
    
    let title: String?
    
    let calories: Double?
    
    let description: String?
    
    var photo: Asset?
    
    var chef: Link?
    
    var tags: [Link]?
    
    var updatedAt: Date?
    
    var createdAt: Date?
    
    var localeCode: String?
    
    public required init(from decoder: Decoder) throws {
        let sys = try decoder.sys()
        self.id = sys.id
        self.localeCode = sys.locale
        self.updatedAt = sys.updatedAt
        self.createdAt = sys.createdAt
        let fields = try decoder.contentfulFieldsContainer(keyedBy: Recipe.FieldKeys.self)
        self.title = try fields.decodeIfPresent(String.self, forKey: .title)
        self.calories = try fields.decodeIfPresent(Double.self, forKey: .calories)
        self.description = try fields.decodeIfPresent(String.self, forKey: .description)
        
        try fields.resolveLink(forKey: .photo, decoder: decoder) { [weak self] linkedAsset in
            self?.photo = linkedAsset as? Asset
        }
        
        self.chef = try fields.decodeIfPresent(Link.self, forKey: .chef)
        self.tags = try fields.decodeIfPresent([Link].self, forKey: .tags)
    }
    
    enum FieldKeys: String, CodingKey {
        case
        title,
        calories,
        description,
        photo,
        chef,
        tags
    }
}
