//
//  Chef.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Contentful

final class Chef: EntryDecodable, FieldKeysQueryable {
    
    static let contentTypeId: String = "chef"
    
    var id: String
    
    var updatedAt: Date?
    
    var createdAt: Date?
    
    var localeCode: String?
    
    var name: String?
    
    public required init(from decoder: Decoder) throws {
        let sys = try decoder.sys()
        self.id = sys.id
        self.localeCode = sys.locale
        self.updatedAt = sys.updatedAt
        self.createdAt = sys.createdAt
        let fields = try decoder.contentfulFieldsContainer(keyedBy: Chef.FieldKeys.self)
        self.name = try fields.decodeIfPresent(String.self, forKey: .name)
    }
    
    enum FieldKeys: String, CodingKey {
        case
        name
    }
}
