//
//  MainPageViewModel.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct MainPageViewModel {
    
    let id: String
    
    let recipeTitle: String
    
    let imageURL: URL?
}
