//
//  MainPageViewController.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

class MainPageViewController: UIViewController {
    
    // MARK: - Constants
    
    let cellIdentifier = typeName(type: MainPageCellView.self)
    
    let itemHeight: CGFloat = 230.0
    
    let defaultInset: CGFloat = 1.5

    // MARK: - Properties
    
    @IBOutlet private weak var collectionView: UICollectionView!
        
    private var refreshControl = UIRefreshControl()
    
    private var viewModels = [MainPageViewModel]()
    
    var presenter: MainPageViewToPresenter?

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        refreshData()
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: MainPageViewController")
    }

    // MARK: - Private functions
    
    private func configureUI() {
        let nib = UINib(nibName: cellIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        configurePullToRefreshControl()
    }
    
    private func configurePullToRefreshControl() {
        collectionView.alwaysBounceVertical = true
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshData() {
        presenter?.updateRecipeListView(completion: { [weak self] viewModels in
            self?.viewModels = viewModels
            DispatchQueue.main.async { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.collectionView.reloadData()
            }
        })
    }
}

extension MainPageViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MainPageCellView
        cell.updateContentView(with: viewModels[indexPath.row])
        cell.layer.cornerRadius = 8
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
}

extension MainPageViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewModel = viewModels[indexPath.row]
        presenter?.selectRecipe(with: viewModel.id)
    }
}

extension MainPageViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - defaultInset * 2, height: itemHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return defaultInset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return defaultInset
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}

extension MainPageViewController: MainPageControllerToRouter {
    
    func presentDetailPage(_ controller: DetailPageViewController) {
        navigationController?.show(controller, sender: nil)
    }
}
