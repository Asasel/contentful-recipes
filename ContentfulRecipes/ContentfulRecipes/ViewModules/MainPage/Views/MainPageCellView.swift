//
//  MainPageCellView.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit
import Nuke

class MainPageCellView: UICollectionViewCell {
    
    // MARK: - Properties
    
    @IBOutlet private weak var imageView: UIImageView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Functions
    
    func updateContentView(with viewModel: MainPageViewModel) {
        titleLabel.text = viewModel.recipeTitle
        guard let imageURL = viewModel.imageURL else {
            return
        }
        Nuke.loadImage(with: imageURL, into: imageView)
    }
}
