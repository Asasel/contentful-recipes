//
//  RecipeService.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

final class RecipeService {
    
    // MARK: - Properties
    
    private var recipeInfoListQueue = OperationQueue()

    // MARK: - Functions
    
    func fetchRecipes(completion: @escaping RecipeInfoListInfoClosure) {
        let recipeListOperation = RecipeListOperation()
        let chefListOperation = ChefListOperation()
        let tagOperation = TagListOperation()
        var operations = [recipeListOperation, chefListOperation, tagOperation]
        let finishOperation = FinishOperation(completion: completion)
        operations.forEach { operation in
            finishOperation.addDependency(operation)
        }
        operations.append(finishOperation)
        recipeInfoListQueue.addOperations(operations, waitUntilFinished: false)
    }
}
