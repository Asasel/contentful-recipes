//
//  RecipeAdaptor.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct RecipeInfoAdaptor {
    
    // MARK: - Properties
        
    private var recipesRaw: [Recipe]
    
    private var chefsRaw: [Chef]
    
    private var tagsRaw: [Tag]
    
    var recipeInfoList: [RecipeInfo] {
        var recipes = [RecipeInfo]()
        recipesRaw.forEach { item in
            let recipe = RecipeInfo(id: item.id,
                       recipeTitle: item.title ?? "",
                       imageURL: item.photo?.url,
                       description: item.description ?? "",
                       chefName: chefsRaw.filter { $0.id == item.chef?.id }.first?.name ?? "",
                       tags: tagsRaw.filter { (item.tags?.compactMap { $0.id } ?? []).contains($0.id) }.compactMap { $0.name ?? "" })
            recipes.append(recipe)
        }
        return recipes
    }
    
    // MARK: - Constructor
    
    init(recipesRaw: [Recipe], chefsRaw: [Chef], tagsRaw: [Tag]) {
        self.recipesRaw = recipesRaw
        self.chefsRaw = chefsRaw
        self.tagsRaw = tagsRaw
    }
}
