//
//  ChefListOperation.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Contentful

typealias ChefListClosure = (([Chef]) -> Void)

protocol ChefListPassProtocol {
    
    var chefList: [Chef] { get }
}

class ChefListOperation: AsyncOperation {
    
    // MARK: - Properties
    
    private var outputChefList = [Chef]()
    
    private lazy var client: Client = {
        let client = Client(spaceId: "kk2bw5ojx476",
        environmentId: "master",
        accessToken: "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c",
        contentTypeClasses: [Chef.self])
        return client
    }()
    
    // MARK: - Construction

    override init() {
        super.init()
    }
    
    // MARK: - Functions
    
    override func main() {
        guard !isCancelled else {
            state = .finished
            return
        }
        fetchChefList { [weak self] chefList in
            guard let strongSelf = self else {
                return
            }
            strongSelf.outputChefList = chefList
            strongSelf.state = .finished
        }
    }

    override func cancel() {
        super.cancel()
    }
    
    private func fetchChefList(completion: @escaping ChefListClosure) {
        client.fetchArray(of: Chef.self) { (result: Result<HomogeneousArrayResponse<Chef>>) in
            switch result {
            case .success(let entry):
                completion(entry.items)
            case .error(let error):
                print("Error \(error)!")
                completion([])
            }
        }
    }

    deinit {
        Log.message("DEINIT: RecipeListOperation")
    }
}

extension ChefListOperation: ChefListPassProtocol {
    
    var chefList: [Chef] {
        return outputChefList
    }
}
