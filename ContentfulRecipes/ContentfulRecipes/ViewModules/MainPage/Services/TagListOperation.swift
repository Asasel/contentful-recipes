//
//  TagListOperation.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Contentful

typealias TagListClosure = (([Tag]) -> Void)

protocol TagListPassProtocol {
    
    var tagList: [Tag] { get }
}

class TagListOperation: AsyncOperation {
    
    // MARK: - Properties
    
    private var outputTagList = [Tag]()
    
    private lazy var client: Client = {
        let client = Client(spaceId: "kk2bw5ojx476",
        environmentId: "master",
        accessToken: "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c",
        contentTypeClasses: [Tag.self])
        return client
    }()
    
    // MARK: - Construction

    override init() {
        super.init()
    }
    
    // MARK: - Functions
    
    override func main() {
        guard !isCancelled else {
            state = .finished
            return
        }
        fetchTagList { [weak self] chefList in
            guard let strongSelf = self else {
                return
            }
            strongSelf.outputTagList = chefList
            strongSelf.state = .finished
        }
    }

    override func cancel() {
        super.cancel()
    }
    
    private func fetchTagList(completion: @escaping TagListClosure) {
        client.fetchArray(of: Tag.self) { (result: Result<HomogeneousArrayResponse<Tag>>) in
            switch result {
            case .success(let entry):
                completion(entry.items)
            case .error(let error):
                print("Error \(error)!")
                completion([])
            }
        }
    }

    deinit {
        Log.message("DEINIT: TagListOperation")
    }
}

extension TagListOperation: TagListPassProtocol {
    
    var tagList: [Tag] {
        return outputTagList
    }
}
