//
//  RecipeListOperation.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Contentful

typealias RecipeListClosure = (([Recipe]) -> Void)

protocol RecipeListPassProtocol {
    
    var recipeList: [Recipe] { get }
}

class RecipeListOperation: AsyncOperation {
    
    // MARK: - Properties
    
    private var outputRecipeList = [Recipe]()
    
    private lazy var client: Client = {
        let client = Client(spaceId: "kk2bw5ojx476",
        environmentId: "master",
        accessToken: "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c",
        contentTypeClasses: [Recipe.self])
        return client
    }()
    
    // MARK: - Construction

    override init() {
        super.init()
    }
    
    // MARK: - Functions

    override func main() {
        guard !isCancelled else {
            state = .finished
            return
        }
        fetchRecipeList { [weak self] recipeList in
            guard let strongSelf = self else {
                return
            }
            strongSelf.outputRecipeList = recipeList
            strongSelf.state = .finished
        }
    }

    override func cancel() {
        super.cancel()
    }
    
    private func fetchRecipeList(completion: @escaping RecipeListClosure) {
        client.fetchArray(of: Recipe.self) { (result: Result<HomogeneousArrayResponse<Recipe>>) in
            switch result {
            case .success(let entry):
                completion(entry.items)
            case .error(let error):
                print("Error \(error)!")
                completion([])
            }
        }
    }

    deinit {
        Log.message("DEINIT: RecipeListOperation")
    }
}

extension RecipeListOperation: RecipeListPassProtocol {
    
    var recipeList: [Recipe] {
        return outputRecipeList
    }
}
