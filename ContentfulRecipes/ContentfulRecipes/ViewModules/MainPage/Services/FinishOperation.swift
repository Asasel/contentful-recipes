//
//  FinishOperation.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias RecipeInfoListInfoClosure = (([Recipe], [Chef], [Tag]) -> Void)

class FinishOperation: AsyncOperation {
    
    // MARK: - Properties
    
    private var recipeListPassProtocol: [RecipeListPassProtocol] {
        return dependencies.filter { $0 is RecipeListPassProtocol }.compactMap { $0 as? RecipeListPassProtocol }
    }
    
    private var chefListPassProtocol: [ChefListPassProtocol] {
        return dependencies.filter { $0 is ChefListPassProtocol }.compactMap { $0 as? ChefListPassProtocol }
    }
    
    private var tagListPassProtocol: [TagListPassProtocol] {
        return dependencies.filter { $0 is TagListPassProtocol }.compactMap { $0 as? TagListPassProtocol }
    }
    
    private var recipesRaw: [Recipe] {
        var recipes = [Recipe]()
        recipeListPassProtocol.forEach { item in
            recipes.append(contentsOf: item.recipeList)
        }
        return recipes
    }
    
    private var chefsRaw: [Chef] {
        var chefs = [Chef]()
        chefListPassProtocol.forEach { item in
            chefs.append(contentsOf: item.chefList)
        }
        return chefs
    }
    
    private var tagsRaw: [Tag] {
        var tags = [Tag]()
        tagListPassProtocol.forEach { item in
            tags.append(contentsOf: item.tagList)
        }
        return tags
    }
    
    private var completion: RecipeInfoListInfoClosure
    
    // MARK: - Construction
    
    init(completion: @escaping RecipeInfoListInfoClosure) {
        self.completion = completion
    }
    
    // MARK: - Functions
    
    override func main() {
        guard !isCancelled else {
            state = .finished
            return
        }
        completion(recipesRaw, chefsRaw, tagsRaw)
        state = .finished
    }

    override func cancel() {
        super.cancel()
    }

    deinit {
        Log.message("DEINIT: FinishOperation")
    }
}
