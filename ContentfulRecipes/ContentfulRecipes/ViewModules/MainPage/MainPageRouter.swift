//
//  MainPageRouter.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

enum MainPageNavigation {
    case
    detailPage
}

class MainPageRouter {
    
    // MARK: - Properties
    
    private weak var contentViewController: MainPageControllerToRouter?
    
    private(set) var mainPageNavigationController: UINavigationController
    
    private var detailPageAssembly: DetailPageAssembly?

    // MARK: - Constructor
    
    init(contentViewController: MainPageViewController, title: String) {
        self.contentViewController = contentViewController
        self.mainPageNavigationController = UINavigationController.instantiateNavigationController(rootViewController: contentViewController, title: title)
        self.mainPageNavigationController.restorationIdentifier = typeName(type: MainPageViewController.self)
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: MainPageRouter")
    }

    // MARK: - Private functions
    
    private func presentDetailPage(with detailPageViewModel: DetailPageViewModel) {
        detailPageAssembly = DetailPageAssembly(with: detailPageViewModel)
        guard let viewController = detailPageAssembly?.viewController else {
            return
        }
        contentViewController?.presentDetailPage(viewController)
    }
}

extension MainPageRouter: MainPagePresenterToRouter {
    
    func navigate(to destination: MainPageNavigation, with detailPageViewModel: DetailPageViewModel) {
        switch destination {
        case .detailPage:
            presentDetailPage(with: detailPageViewModel)
        }
    }
}
