//
//  MainPageAssembly.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

class MainPageAssembly {
    
    // MARK: - Properties
    
    private var presenter: MainPagePresenter

    private var viewController: MainPageViewController

    private var interactor: MainPageInteractor

    private var router: MainPageRouter
    
    var mainPageNavigationController: UINavigationController {
        return router.mainPageNavigationController
    }

    // MARK: - Constructor
    
    init() {
        self.viewController = MainPageViewController.instantiateFromNib()
        self.interactor = MainPageInteractor()
        self.router = MainPageRouter(contentViewController: self.viewController, title: "Recipes")
        self.presenter = MainPagePresenter(router: self.router, interactor: self.interactor)
        self.viewController.presenter = self.presenter
    }

    // MARK: - Functions
    
    deinit {
        Log.message("DEINIT: MainPageAssembly")
    }
}
