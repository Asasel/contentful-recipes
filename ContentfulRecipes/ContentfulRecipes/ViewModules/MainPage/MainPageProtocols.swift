//
//  MainPageProtocols.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias MainPageUpdateViewClosure = ([MainPageViewModel]) -> Void

protocol MainPageViewToPresenter: class {   // View calls, Presenter listens.
    
    func updateRecipeListView(completion: @escaping MainPageUpdateViewClosure)
    
    func selectRecipe(with id: String)
}

protocol MainPagePresenterToRouter: class {    // Presenter calls, Router listens.
    
    func navigate(to destination: MainPageNavigation, with detailPageViewModel: DetailPageViewModel)
}

protocol MainPageControllerToRouter: class {
    
    func presentDetailPage(_ controller: DetailPageViewController)
}

protocol MainPagePresenterToInteractor: class {   // Presenter calls, Interactor listens.
    
    func updateRecipeList(completion: @escaping RecipeInfoListInfoClosure)
}
