//
//  Log.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class Log {

    // MARK: - Constants

    static let showLog = true

    // MARK: - Construction

    static func message(_ message: String) {
        guard showLog else {
            return
        }
        print(message)
    }
}
