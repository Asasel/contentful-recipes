//
//  AsyncOperation.swift
//  ContentfulRecipes
//
//  Created by Angelina Latash on 3/3/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class AsyncOperation: Operation {

    // MARK: - Nested

    enum State: String {
        case ready, executing, finished

        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }

    // MARK: - Properties

    override var isAsynchronous: Bool {
        return true
    }

    override var isReady: Bool {
        return super.isReady && state == .ready
    }

    override var isExecuting: Bool {
        return state == .executing
    }

    override var isFinished: Bool {
        return state == .finished
    }

    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }

    // MARK: - Functions

    override func start() {
        if isCancelled {
            state = .finished
            return
        }
        state = .ready
        main()
    }
}
