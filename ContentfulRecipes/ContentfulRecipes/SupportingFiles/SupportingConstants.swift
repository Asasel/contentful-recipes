//
//  SupportingConstants.swift
//  Contentful
//
//  Created by Angelina Latash on 3/2/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

let defaultErrorText = "An error occurred during the request.\nPlease, try later..."

// MARK: - Functions

func typeName(type: Any) -> String {
    return String(describing: type.self)
}
